# Tretje vaje Elm@FRI

V tretjem tednu se spoznamo z delom s tipom Maybe in seznami.

## Osnovno delo s seznami

Implementirajte spodnje funkcije za delo s seznami. Pri tem lahko uporabljate iz tipa `List` zgolj funkcije `head` in `tail`.

```elm
get: Int -> List Int -> Maybe Int
find: Int -> List Int -> Maybe Int
delete: Int -> List Int -> List Int
concat: List Int -> List Int -> List Int
map: List Int -> (Int->Int) -> List Int
foldLeft: Int -> List Int -> (Int->Int->Int) -> Int
```
- Funkcija 'get' dobi na vhod indeks in seznam, vrne pa element na tistem indeksu, oziroma 'Nothing', če je indeks izven obsega.
- Funkcija 'find' pa poišče podani element in vrne indeks njegove prve pojavitve, oz. 'Nothing', če elementa ni v seznamu.
- Funkcija 'delete' dobi en element, ki ga je potrebno odstraniti iz seznama (vse pojavitve). Če elementa ni v seznamu, potem seznam ohrani enak.
- Funkcija 'concat' vzame dva seznama in vrne nov seznam staknjen z drugim.
- Funkcija 'map' vzame seznam in funkcijo, ki preslika en element v drugega, ter vrne nov seznam, kjer je vsak element originalnega seznama preslikan s podano funkcijo.
- Funkcija 'foldLeft' vzame en začetni element (stanje), seznam, ter funkcijo, ki združuje en element s trenutnim stanjem - in to od leve proti desni.




## Kodiranje s pomočjo seznamov
Napišite funkcijo, ki bo zakodirala niz znakov s tako imenovanim LU/MTF kodiranjem.

1. LU/MTF (List Update/ Move To Front) kodiranje izgleda tako, da vsak znak zakodiramo z njegovo pozicijo v nekem seznamu.
2. Ta seznam ni statičen, ampak ga vsak dostop do elementa spremeni - zato je to List Update.
3. Seznam pa spremenimo tako, da vsak element, ki smo ga našli premaknemo na začetek (zato je to Move To Front).
4. Denimo, da bodo nizi podani kot zaporedje ASCII znakov, zato lahko na začetku zgradimo seznam, v katerem so 	vsi ASCII znaki od 0 do 255. Za vsak znak (iz zaporedja, ki ga kodiramo) poiščemo njegov indeks v kodirnem seznamu
in ta seznam ustrezno popravimo.

```elm
startList: List Int
```
To je začetni seznam, ki hrani vrednosti od 0 do 255 (v začetku v tem vrstnem redu).


```elm
findAndMTF: Char -> List Int -> Maybe (Int, List Int)
```
Ta funkcija sprejme en znak in kodirni seznam, vrne pa pozicijo podanega znaka v tem seznamu, ter ustrezno spremenjen seznam.



```elm
encodeMTF: String -> Maybe (List Int)
```
S pomočjo prejšnje funkcije implementirajte kodiranje LU/MTF. sprejmete torej niz, ter vrnete seznam celih števil (oz. Nothing, če kodiranje ne uspe).


:crown:
Implementirajte še dekodiranje - sprejmete torej seznam celih števil in vrnete niz, ki je bil zakodiran z metodo LU/MTF.
```elm
decodeMTF: List Int -> Maybe String
```
