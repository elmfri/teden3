module Main exposing (..)

import Html exposing (..)


-- get: Int -> List Int -> Maybe Int
-- find: Int -> List Int -> Maybe Int
-- delete: Int -> List Int -> List Int
-- concat: List Int -> List Int
-- map: List Int -> (Int->Int) -> List Int
-- foldLeft: Int -> List Int -> (Int->Int->Int) -> Int
---------------------------------------------------------------
-- startList: List Int
-- findAndMTF: Char -> List Int -> Maybe (Int, List Int)
-- encodeMTF: String -> Maybe (List Int)
-- decodeMTF: List Int -> Maybe String


main =
    Html.text "Tukaj pretestirajte svoje funkcije"
